# Generated by Django 2.1 on 2019-01-25 18:46

from django.db import migrations, models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0008_auto_20190118_2151'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from=['title'])),
                ('content', models.TextField()),
                ('is_published', models.BooleanField(default=False)),
                ('position', models.FloatField(default=0)),
            ],
            options={
                'ordering': ['position'],
            },
        ),
    ]
