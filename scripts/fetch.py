"""Fetch exercises from hackinscience API.
"""

import argparse
from getpass import getpass
from pathlib import Path
import json

import requests


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--username")
    parser.add_argument("--password")
    parser.add_argument(
        "--endpoint", default="https://www.hackinscience.org/api/exercises/"
    )
    return parser.parse_args()


def fix_newline_at_end_of_file(text):
    if not text:
        return text
    if text[-1] != "\n":
        return text + "\n"
    return text


def main():
    args = parse_args()
    if not args.username:
        args.username = input("Username: ")
    if not args.password:
        args.password = getpass()
    next_exercise_page = args.endpoint
    while next_exercise_page:
        exercises = requests.get(
            next_exercise_page, auth=(args.username, args.password)
        ).json()
        if "results" not in exercises:
            print(exercises)
            exit(1)
        translatables = []
        for exercise in exercises["results"]:
            path = Path("exercises") / exercise["slug"]
            path.mkdir(exist_ok=True, parents=True)
            translatables.append(exercise["wording"])
            translatables.append(exercise["title"])
            for file in "check.py", "solution.py", "wording.md", "initial_solution.py":
                (path / (file)).write_text(
                    fix_newline_at_end_of_file(exercise[file.split(".")[0]])
                )
                del exercise[file.split(".")[0]]
            (path / "meta").write_text(json.dumps(exercise, indent=4))
        next_exercise_page = exercises["next"]
    with open("to_translate.py", "w", encoding="UTF-8") as to_translate:
        for string in translatables:
            to_translate.write(f"_({string!r})\n")


if __name__ == "__main__":
    main()
